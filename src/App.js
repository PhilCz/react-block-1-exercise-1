import logo from './logo.svg';
import './App.css';

const App = () => {
  const categories = ["t-shirts", "hats", "shorts", "shirts", "pants", "shoes"]
  return(
    <div>
        {categories.map((product, i) => {
          return <h3 key ={i}> You have {product} </h3>
        })}
    </div>
  );
};

export default App;
